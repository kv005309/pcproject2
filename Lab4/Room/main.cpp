#include <iostream>
#include "Character.hpp"
#include "Area.hpp"

int gameLoop(Player player) {
	int option;
	bool loop = true;
	
	while (loop) {
		std::cout << "0:View connections\n1:View items\n2:Move\n3:Quit\n4:View Room\n5:Interact\n";

		std::cin >> option;
		if (option == 0)// View connections
		{
			std::cout << player.getConnections();
		}
		else if (option == 1)// View items in room
		{
			std::cout<<player.viewItems();
		}
		else if (option == 2)// Move
		{
			std::cout << "Where would you like to move?";
			std::string location;
			std::cin >> location;
			player.moveTo(location);
		}
		else if (option == 3)// Quit
		{
			loop = !loop; // Falsifies
		}
		else if (option == 4)// View Room
		{
			std::cout << player.viewRoom()<<"\n";
		}
		else if (option == 5)// Interact with item
		{
			std::cout << "Where would you like to interact with?";
			std::string itemname;
			std::cin >> itemname;
			std::cout<<player.interactWith(itemname);
		}
	}

	return 0;
}

int main() {
	//Room startingRoom{"This room is bland"};
	//Room waterfall{ "A waterfall rushes down the room"};
	//Item cuboid{ "Cuboid","has sides A, B and C" };
	//startingRoom.AddExit("North",&waterfall);
	//startingRoom.AddItem(cuboid);

	
	Area game_area{};
	game_area.loadMapFromFile("map.txt");

	bodypart head = bodypart{ 4,6,true,false,true,false,false };

	Player human1{ "Bob",5,game_area.getRoom("room1")};
	human1.assignBodyPart(head);
	gameLoop(human1);

	return 0;
}