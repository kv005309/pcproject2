#include <iostream>
#include <vector>
#include "Room.hpp"
#include "Item.hpp"
#include <map>
#include <utility>


Room::Room(const std::string & desc)
{
	this->description = desc;
	
}

std::string Room::GetDescription()
{
	return description;
}

std::string Room::viewItems() {
	std::string viewString;
	if (items.size() != 0) {
		for (int i = 0; i < items.size();i++)
			viewString += items[i].getName() + " \n";

	}
	return viewString;
}

std::string Room::interactWithItem(std::string itemName) {
	std::string viewString;
	if (items.size() != 0) {
		for (int i = 0; i < items.size();i++)
			if (items[i].getName() == itemName) {
				viewString += items[i].Interact() + " \n";
			};
	}
	return viewString;
}

void Room::AddItem(const Item& item)
{
	//items.resize(items.size() + 1);
	//items.insert(items.end(), item);
	items.push_back(item);
}

void Room::RemoveItem(const Item &item)
{
	auto foundItem = std::find(items.begin(), items.end(), item);
	if (foundItem != items.end()) {
		items.erase(foundItem);
	}
	else {
		std::cout << "Item not found";
	}
}

void Room::AddExit(std::string exitName, Room*  roomAdded) {
	exits[exitName] = roomAdded;
}

std::string Room::viewExits() {
	std::string output = "Exits:\n";

	for (auto const& x : exits) {
		output.append(x.first+"\n");

	}
	return output;
}

Room* Room::getExit(std::string input) {
	return(exits.at(input));
}