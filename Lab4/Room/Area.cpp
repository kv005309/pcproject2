#include "Area.hpp"
#include <iostream>
#include <map>
#include <fstream>
#include <string>
#include <vector>

//	std::map<std::string, Room*> rooms;

Area::Area() {
}



void Area::addRoom(const std::string& name, Room* room) {
	rooms[name] = room;
} // Adds a room and uses name as key

Room* Area::getRoom(const std::string& name) {
	return rooms[name];
} // Retrieve a room by name

void Area::connectRooms(const std::string& room1name, const std::string& room2name, const std::string& direction) {
	rooms[room1name]->AddExit(direction, rooms[room2name]);
} // Connect two rooms using specified direction (one way)

void Area::loadMapFromFile(const std::string& filename) {
	// Load the game map from a text file, creating rooms and connections

	std::ifstream map_file(filename);

	std::string line;

	while (std::getline(map_file, line)) { // One line at a time
		std::vector<std::string> words_in_line;
		int start_pos = 0;
		while (line.size() != 0) {//while line is not empty

			if (line.find("|") != std::string::npos) // Search through the text to get the rooms and connection name.
			{
				int find_pos = line.find("|");
				words_in_line.push_back(line.substr(0, find_pos));
				line.erase(start_pos, find_pos + 1);
			}
			else {
				words_in_line.push_back(line.substr(0, line.size()));
				line.erase(start_pos, line.size());

			}

		}

		// Now initialise the rooms from the gotten values, checking if they don't already exist.
		// Check room 1 exists:

		if (rooms.find(words_in_line[0]) == rooms.end()) {
			Room * room1 = new Room(words_in_line[3]);// If not, instanciate
			rooms[words_in_line[0]] = room1;
			// Check room 2 exists
			if (rooms.find(words_in_line[1]) == rooms.end()) {
				Room* room2 = new Room(words_in_line[4]);// If not, instanciate with dynamic allocation
				rooms[words_in_line[1]] = room2;
				// Add exit
				room1->AddExit(words_in_line[2], room2);

			}
			else {
				Room* room2 = rooms[words_in_line[1]];
				// Add exit
				room1->AddExit(words_in_line[2], room2);

			}
		}
		else {
			Room* room1 = rooms[words_in_line[0]];
			// Check room 2 exists
			if (rooms.find(words_in_line[1]) == rooms.end()) {
				Room * room2 = new Room(words_in_line[4]);// If not, instanciate
				rooms[words_in_line[1]] = room2;
				// Add exit
				room1->AddExit(words_in_line[2], room2);

			}
			else {
				Room* room2 = rooms[words_in_line[1]];
				// Add exit
				room1->AddExit(words_in_line[2], room2);

			}

		}
	}

}