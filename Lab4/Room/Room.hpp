#pragma once
#include <iostream>
#include <map>
#include <vector>
#include "Item.hpp"


class Room {
private:
	std::string description;
	std::map<std::string, Room*> exits;
	std::vector<Item> items;

public:
	Room(const std::string& description);
	std::string GetDescription();
	std::string viewItems();
	void AddItem(const Item& item);
	void RemoveItem(const Item& item);
	void AddExit(std::string exitName,Room* exit);
	std::string viewExits();
	Room* getExit(std::string input);
	std::string interactWithItem(std::string itemName);
};