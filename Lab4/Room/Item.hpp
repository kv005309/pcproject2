#pragma once
#include <iostream>

class Item {
private:
	std::string name;
	std::string description;
public:
	Item(const std::string name, const std::string description);
	~Item();
	bool operator == (Item b);
	std::string Interact();
	std::string getName();
};