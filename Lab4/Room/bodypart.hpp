#pragma once

#include <iostream>
#include <random>


class bodypart {
private:
	int health;
	
	bool dodgeValue;

	bool necessary;
	bool providesLocomotion;
	bool providesManipulation;
	bool bleeding;
	bool hasBlood;
	bool broken;
	bool hasBones;
	
	bool dead;

	std::string type;
	std::string name;
	


public:
	bodypart(int startingHealth,int dodgeValue,bool canBleed,bool canBreak, bool necessary, bool providesLocomotion, bool providesManipulation);
	bool isNecessary();
	bool hasLocomotion();
	bool hasManipulation();
	bool canBleed();
	bool canBreak();
	bool isBleeding();
	bool isBroken();
	std::string getType();
	std::string getName();
	bool getHit(int hitModifier); // Called by an attacker to determine if the attack actually hits or not.
	void takeDamage(int healthReduction); // Called by an attacker with an attack to deal damage to a part, only damage needed, rest of calculation happens in here.
	~bodypart();
};