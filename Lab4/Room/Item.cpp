#include <iostream>
#include <string>
#include "Item.hpp"



Item::Item(const std::string name,const std::string description) {
	this->name = name;
	this->description = description;

}

Item::~Item()
{
}

bool Item::operator==(Item b) {
	if (name.compare(b.name) == 0 && description.compare(b.description) == 0){
		return true;
	}
	else {
		return false;
	}
}

std::string Item::Interact() {
	return description;
}

std::string Item::getName() {
	return name;
}