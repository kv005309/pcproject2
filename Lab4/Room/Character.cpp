#include "Character.hpp"
#include <iostream>

Character::Character(const std::string& name, int health) {

	this->name = name;
	this->health = health;

}

void Character::takeDamage(int damageTaken) {
	health -= damageTaken;
}

Player::Player(const std::string& mname, int mhealth, Room* startingRoom)
	: Character(mname, mhealth) {
	std::cout << "Player " << mname << " instantiated.\n" << startingRoom->GetDescription()<<"\n";
	location = startingRoom;
}
// Idea is to add hidden connections, by adding a property to a room "hidden" that will tell it if it shows up when connections are read out.
std::string Player::getConnections() {
	return location->viewExits();
}

std::string Player::viewRoom() {
	return location->GetDescription();
}

std::string Player::viewItems() {
	return location->viewItems();
}

std::string Player::interactWith(std::string itemName) {
	return(location->interactWithItem(itemName));
}

void Player::moveTo(std::string desiredlocation) {
	// Take location, and move player there.
	location = location->getExit(desiredlocation);
}

void Player::assignBodyPart(bodypart newPart) {
	parts.push_back(newPart);
}
