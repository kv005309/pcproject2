#include "bodypart.hpp"


bodypart::bodypart(int startingHealth, int dodgeValue,bool canBleed, bool canBreak, bool necessary, bool providesLocomotion, bool providesManipulation) {
	this->health = startingHealth;
	this->dodgeValue = dodgeValue;
	this->hasBlood = canBleed;
	this->hasBones = canBreak;
	this->necessary = necessary;
	this->providesLocomotion = providesLocomotion;
	this->providesManipulation = providesManipulation;

	
	this->broken = false;
	this->bleeding = false;

	this->dead = false;

}

bool bodypart::isNecessary() {
	return necessary;
}

bool bodypart::canBleed() {
	return hasBlood;
}

bool bodypart::canBreak() {
	return hasBones;
}

bool bodypart::hasLocomotion() {
	return providesLocomotion;
}

bool bodypart::hasManipulation() {
	return providesManipulation;
}

bool bodypart::isBleeding() {
	return bleeding;
}

bool bodypart::isBroken() {
	return broken;
}

std::string bodypart::getType() {
	return type;
}

std::string bodypart::getName() {
	return name;
}

bool bodypart::getHit(int hitModifier) {
	int rollNumber = std::rand() % 20 + 1 + hitModifier - dodgeValue;
	if (rollNumber > 10) {
		return true;
	};
	return false;
}

void bodypart::takeDamage(int healthReduction) {
	if (healthReduction >= (health / 2)) {
		if (canBleed()) {
			bleeding = true;
		}
		if (canBreak()) {
			broken = true;
		}
	}
	health -= healthReduction;

	if (health <= 0) {
		//die
		dead = true;

	}
}

bodypart::~bodypart() {}
