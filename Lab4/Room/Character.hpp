#pragma once

#include <iostream>
#include <vector>
#include "Room.hpp"
#include "bodypart.hpp"

class Character {
private:
	int health;
	std::vector<Item> inventory;
protected:
	std::string name;

public:
	Character(const std::string& name, int health);
	void takeDamage(int damage);

};

class Player : private Character {
private:
	Room* location;
	std::vector<bodypart> parts;
public:
	Player(const std::string& name, int health, Room* startingRoom);
	std::string getConnections();
	std::string viewRoom();
	std::string interactWith(std::string itemName);
	void moveTo(std::string input);
	std::string viewItems();
	void assignBodyPart(bodypart newPart);
};