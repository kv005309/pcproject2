#pragma once

#include "Room.hpp"
#include <iostream>
#include <map>


class Area {

private:
	std::map<std::string, Room*> rooms;

public:
	Area();
	void addRoom(const std::string& name, Room* room); // Adds a room and uses name as key
	Room* getRoom(const std::string& name); // Retrieve a room by name
	void connectRooms(const std::string& room1name, const std::string& room2name, const std::string& direction); // Connect two rooms using specified direction (one way)
	void loadMapFromFile(const std::string& filename); // Load the game map from a text file, creating rooms and connections

};